/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

/**
 * Clase dedicada a declarar valores constantes del sistema
 *
 * @author ValeriaRo
 */
public class Constantes {

    public static final String URL = "https://test-externalrgw.ar.sitrack.com/frame";
    public static final String METHOD = "PUT";
    public static final String CONTENTTYPE = "application/json";
    public static final String SECRETKEY = "ccd517a1-d39d-4cf6-af65-28d65e192149";
    public static final String APPLICATION = "ReportGeneratorTest";
    public static final String FORMATOFECHAS = "yyyy-MM-ddHH:mm:ssZ";

}
