/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package modelo;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author ValeriaRo
 */
public class Reporte {

    private DateFormat formatDate = new SimpleDateFormat(Constantes.FORMATOFECHAS);
    private String logingCode;
    private String reportDate;
    private String reportType;
    private double latitude;
    private double longitude;
    private double gpsDop;
    private Integer heading;
    private String speedLabel;
    private Integer gpsSatellites;
    private String text;
    private String textLabel;
    private double speed;

    /**
     * Constructor hardcodeado para practicidad de pruebas
     */
    public Reporte() {
        this.logingCode = "98173";
        this.reportDate = formatDate.format(new Date());
        this.reportType = "2";
        this.latitude = Math.random() * 3600;
        this.longitude = Math.random() * 3600;
        this.gpsDop = Math.random() * 50;
        this.gpsSatellites = 3;
        this.heading = 335;
        this.speed = Math.random() * 100;
        this.speedLabel = "GPS";
        this.text = "Valeria Cerroni";
        this.textLabel = "TAG";
    }

    /**
     * Constructor de parameros dinamicos
     */
    public Reporte(String logingCode, String reportDate, String reportType, double latitude, double longitude, double gpsDop, Integer heading, String speedLabel, Integer gpsSatellites, String text, String textLabel, double speed) {
        this.logingCode = logingCode;
        this.reportDate = reportDate;
        this.reportType = reportType;
        this.latitude = latitude;
        this.longitude = longitude;
        this.gpsDop = gpsDop;
        this.heading = heading;
        this.speedLabel = speedLabel;
        this.gpsSatellites = gpsSatellites;
        this.text = text;
        this.textLabel = textLabel;
        this.speed = speed;
    }

    public String getTextLabel() {
        return textLabel;
    }

    public void setTextLabel(String textLabel) {
        this.textLabel = textLabel;
    }

    public String getLogingCode() {
        return logingCode;
    }

    public void setLogingCode(String logingCode) {
        this.logingCode = logingCode;
    }

    public String getReportDate() {
        return reportDate;
    }

    public void setReportDate(String reportDate) {
        this.reportDate = reportDate;
    }

    public String getReportType() {
        return reportType;
    }

    public void setReportType(String reportType) {
        this.reportType = reportType;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getGpsDop() {
        return gpsDop;
    }

    public void setGpsDop(double gpsDop) {
        this.gpsDop = gpsDop;
    }

    public Integer getHeading() {
        return heading;
    }

    public void setHeading(Integer heading) {
        this.heading = heading;
    }

    public String getSpeedLabel() {
        return speedLabel;
    }

    public void setSpeedLabel(String speedLabel) {
        this.speedLabel = speedLabel;
    }

    public Integer getGpsSatellites() {
        return gpsSatellites;
    }

    public void setGpsSatellites(Integer gpsSatellites) {
        this.gpsSatellites = gpsSatellites;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public double getSpeed() {
        return speed;
    }

    public void setSpeed(double speed) {
        this.speed = speed;
    }

}
