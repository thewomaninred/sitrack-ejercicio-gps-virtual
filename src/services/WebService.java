/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package services;

import java.io.IOException;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import modelo.Constantes;
import org.json.JSONObject;

/**
 *
 * @author ValeleriaRo
 */
public class WebService {

    /**
     * Autenticacion mediante firma digital
     */
    public String autenticationReportIn() throws NoSuchAlgorithmException {
        System.out.println("Autenticando Reporte Entrante");
        String timestamp = String.valueOf(System.currentTimeMillis() / 1000);
        String aux = Constantes.APPLICATION + Constantes.SECRETKEY + timestamp;
        String signature = java.util.Base64.getEncoder().encodeToString(MessageDigest.getInstance("MD5").digest(aux.getBytes()));
        return signature;
    }

    /**
     * Conexion para enviar el registro
     */
    public int upService(JSONObject reporte) throws InterruptedException {
        HttpURLConnection connection = null;
        String authorization = null;
        int responseCode = 0;
        OutputStream outpustream = null;

        try {
            authorization = autenticationReportIn();
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(WebService.class.getName()).log(Level.SEVERE, null, ex);
        }

        try {

            connection = (HttpURLConnection) new URL(Constantes.URL).openConnection();
            connection.addRequestProperty("Authorization", authorization);//me autentique
            connection.addRequestProperty("Accept", Constantes.CONTENTTYPE);
            connection.setConnectTimeout(60000);
            connection.setReadTimeout(30000);
            connection.setRequestMethod(Constantes.METHOD);
            connection.setRequestProperty("Content-Type", Constantes.CONTENTTYPE);

            responseCode = connection.getResponseCode();
            System.out.println(connection.getResponseMessage());

        } catch (MalformedURLException ex) {
            Logger.getLogger(WebService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(WebService.class.getName()).log(Level.SEVERE, null, ex);
        }

        if (responseCode == HttpURLConnection.HTTP_OK) {
            try {
                System.out.println("Enviando Reporte");
                outpustream = connection.getOutputStream();
                outpustream.flush();
                outpustream.write(reporte.toString().getBytes());
                outpustream.flush();
            } catch (IOException ex) {
                Logger.getLogger(WebService.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return responseCode;
    }

}
