/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package sitrackejercicio;

import controlador.GestorReportes;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ValeriaRo
 */
public class SITRACKEjercicio {

//TODO Todas las variables deben ser en ingles, separar en capas
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws InterruptedException {
        // TODO code application logic here
        GestorReportes ejercicioSitrack = new GestorReportes();
        try {
            System.out.println("Ejerciccio para SITRACK en ejecución\n");
            ejercicioSitrack.run();
            System.out.println("\nLa ejecución ha finalizado");
        } catch (NoSuchAlgorithmException ex) {
            Logger.getLogger(SITRACKEjercicio.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
}
