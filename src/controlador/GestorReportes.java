/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controlador;

import java.security.NoSuchAlgorithmException;
import modelo.Reporte;
import org.json.JSONObject;
import services.WebService;

/**
 * Clase experta en comportamientos de los Reportes
 *
 * @author ValeriaRo
 */
public class GestorReportes {

    Reporte reporte = new Reporte();
    JSONObject errorRequest;
    int recuest = 0;

    /**
     * Instaciación y seteo del los valores del reporte en JSON
     */
    public JSONObject buildReport(String logingCode, String reportDate, String reportType, double latitude, double longitude, double gpsDop, Integer heading, String speedLabel, Integer gpsSatellites, String text, double speed, String textLabel) {
        JSONObject reporte = new JSONObject();
        reporte.append("logingCode", logingCode);
        reporte.append("reportDate", reportDate);
        reporte.append("reportType", reportType);
        reporte.append("latitude", latitude);
        reporte.append("longitude", longitude);
        reporte.append("gpsDop", gpsDop);
        reporte.append("gpsSatellites", gpsSatellites);
        reporte.append("heading", heading);
        reporte.append("speed", speed);
        reporte.append("speedLabel", speedLabel);
        reporte.append("text", text);
        reporte.append("textLabel", textLabel);

        return reporte;
    }

    /**
     * Disparador de Microservicio
     */
    public void run() throws NoSuchAlgorithmException, InterruptedException {
        WebService activateConnection = new WebService();
        errorRequest = new JSONObject();
        int turn = 0;
        do {
            recuest = activateConnection.upService(buildReport(reporte.getLogingCode(), reporte.getReportDate(), reporte.getReportType(), reporte.getLatitude(), reporte.getLongitude(), reporte.getGpsDop(), reporte.getHeading(), reporte.getSpeedLabel(), reporte.getGpsSatellites(), reporte.getText(), reporte.getSpeed(), reporte.getTextLabel()));
            if (recuest == 200) {
                System.out.println("Reporte Enviado Exitosamente");
                break;
            } else {

                if (recuest > 99 && recuest < 200) {
                    System.out.println("Información Importante");
                }
                if (recuest > 299 && recuest < 400) {
                    System.out.println("Redireccionando");

                }
                if (recuest > 399 && recuest < 500) {
                    System.out.println("Error de Autenticación");

                }
                if (recuest > 499 && recuest < 600) {
                    System.out.println("Ha ocurrido un Problema en el servidor");

                }
            }
            turn++;
            System.out.println("Intento " + turn + " de 10");
        } while (recuest != 200 && turn != 10);

    }
}
